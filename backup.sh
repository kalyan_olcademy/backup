#!/bin/sh

message="backup-commit from $USER@$(hostname -s) on $(date)"
GIT=`which git`
${GIT} checkout backup
${GIT} add --all .
${GIT} commit -m "$message"

gitPush=$(${GIT} push -vvv git@github.com:kalyan_olcademy/backup.git)
echo "$gitPush"
